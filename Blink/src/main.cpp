#include <Arduino.h>

// put function declarations here:
//int myFunction(int, int);

void setup() {
  // put your setup code here, to run once:
  pinMode(BUILTIN_LED, OUTPUT);  //Initialize the BUILTIN_LED pin as an output
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(BUILTIN_LED, LOW); 
  delay(1000); 
  digitalWrite(BUILTIN_LED, HIGH);
  delay(1000);  
}

// put function definitions here:
// int myFunction(int x, int y) {
//   return x + y;
// }